﻿using System;
#if XBOX
#else
using System.Linq.Expressions;
#endif


namespace Dandelion.Factory.Extensions
{
    public static class GenericExtensions
    {
#if XBOX
#else
        public static void To<TIn, TOut>(this TIn material, Expression<Func<TOut>> expression)
        {
            PlantSchool.Grow<TOut>().From(material).Now(plant => expression.Compile());
        }
#endif
        public static void To<TIn, TOut>(this TIn material, Action<TOut> action)
        {
            PlantSchool.Grow<TOut>().From(material).Now(action);
        }
    }
}